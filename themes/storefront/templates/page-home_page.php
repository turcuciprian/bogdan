<?php
/*
  Template Name: Bogdan Home Page
 */
get_header();
?>

<div id="primary" class="content-area bogdan-home">
    <main id="main" class="site-main" role="main">
        <div class="flexslider">
            <ul class="slides">
                <?php
                $args = array(
                    'post_type' => 'sf_home_gallery',
                    'posts_per_page' => 10
                );
                $query = new WP_Query($args);
// The Loop
                if ($query->have_posts()) {
                    while ($query->have_posts()) {
                        $query->the_post();
                        $post_id = $query->post->ID;

                        $url = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'home_gallery');
                        $caption = '';
                        if (function_exists('get_the_feature_caption')) {
                            $caption = get_the_feature_caption($post_id);
                        }
                        ?>
                        <li>
                            <a href="<?php echo $caption; ?>">
                                <img src="<?php echo $url['0']; ?>" />
                            </a>
                        </li>
                        <?php
                    }
                    wp_reset_postdata();
                }
                ?>
            </ul>
        </div>
        <div class="fs">
            <?php
            $args = array(
                'post_type' => 'sf_fSP',
                'posts_per_page' => 4
            );
            $query = new WP_Query($args);
// The Loop
            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();
                    $post_id = $query->post->ID;
                    $post_title = $query->post->post_title;

                    $url = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'sf_img');
                    $caption = '';
                    if (function_exists('get_the_feature_caption')) {
                        $caption = get_the_feature_caption($post_id);
                    }
                    ?>
                    <div class="block">
                        <h3><?php echo $post_title; ?></h3>
                        <a href="<?php echo $caption; ?>">
                            <img src="<?php echo $url['0']; ?>" />
                        </a>
                    </div>
                    <?php
                }
                wp_reset_postdata();
            }
            ?>

        </div>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>